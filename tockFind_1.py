# coding:utf-8
filePath = 'D:\projects\dataFile\stock.txt'

# 股票名 到 股票信息的 表
name2info = {}
code2info = {}

with open(filePath.replace('\\', '/'), 'r') as f:
    stocks = f.read().splitlines()

    for stock in stocks:
        stock = stock.split('|')
        name = stock[0].strip()
        code = stock[1].strip()
        name2info[name] = '{}:{}'.format(name,code)
        code2info[code] = '{}:{}'.format(name,code)
        # name2info[name] = f'{name}:{code}'
        # code2info[code] = f'{name}:{code}'

while True:
    keywords = raw_input('请输入要查询的股票名称或代码：')
#
    keywords = keywords.strip()
#
    # 如果输入为空
    if not keywords:
        continue

    # 如果是数字，
    if keywords.isdigit():
        # 一定要写全6位股票代码
        if len(keywords) < 6:
            print('请写全6位股票代码')
            continue

        elif keywords in code2info:
            print(code2info[keywords])

        else:
            print('找不到该股票代码')

    # 如果不全是数字，作为股票名称处理
    else:

        if keywords in name2info:
            print(name2info[keywords])

        else:
            print('找不到该股票名称')