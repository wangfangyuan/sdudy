# coding:utf-8
import random
'''
=========================================
+   继续学习教程，学完 自定义类  那一章， 做这题：
+   http://v3.byhy.net/prac/pri546/py/018a0y/
+   如果不知道如何定时结束，可以先不实现定时结束，先实现 循环10次 猜动物喂食
+   另外：课后有个森林打怪的练习，先不要做，先做这个补充练习，做完补充练习后，再做森林打怪的练习
=========================================
练习-自定义类
动物园文字游戏
用面向对象的设计编写一个python程序，实现一个文字游戏系统。

动物园里面有10个房间，房间号从1 到 10。

每个房间里面可能是体重200斤的老虎或者体重100斤的羊。 游戏开始后，系统随机在10个房间中放入老虎或者羊。

然后随机给出房间号，要求游戏者选择敲门还是喂食。

如果选择喂食： 喂老虎应该输入单词 meat，喂羊应该输入单词 grass 喂对了，体重加10斤。 喂错了，体重减少10斤

如果选择敲门： 敲房间的门，里面的动物会叫，老虎叫会显示 ‘Wow !!’,羊叫会显示 ‘mie~~’。 动物每叫一次体重减5斤。

游戏者强记每个房间的动物是什么，以便不需要敲门就可以喂正确的食物。 游戏3分钟结束后，显示每个房间的动物和它们的体重。

其中3分钟结束的实现方法：先获取游戏开始时间，游戏过程中每次用户输入后，再获取时间，减去开始时间， 如果超过3分钟，游戏结束'''


class Tiger:

    def __init__(self, weight, name):
        self.weight = weight
        self.name = name


class Sheep:

    def __init__(self, weight, name):
        self.weight = weight
        self.name = name


rooms = []
# 动物园里面有10个房间，房间号从1 到 10。
for i in range(1, 11):
    rand = random.randint(0, 1)
    # 每个房间里面可能是体重200斤的老虎或者体重100斤的羊。
    if rand == 0:
        rooms.append(Tiger(200, 'tiger'))
    else:
        rooms.append(Sheep(100, 'sheep'))

i = 0
while i < 3:
    # 游戏开始后，系统随机在10个房间中放入老虎或者羊。
    animal = random.choice(rooms)
    choose = raw_input('请选择敲门还是喂食：')
    # 如果选择喂食： 喂老虎应该输入单词meat，喂羊应该输入单词grass喂对了，体重加10斤。 喂错了，体重减少10斤
    if choose == '喂食':
        chooseFood = raw_input('喂老虎请输入meat，喂羊请输入grass：')
        if chooseFood == 'meat':
            if 'Tiger' == animal.name:
                animal.weight += 10
            else:
                animal.weight -= 10
        elif chooseFood == 'grass':
            if 'Sheep' == animal.name:
                animal.weight += 10
            else:
                animal.weight -= 10

    # 如果选择敲门： 敲房间的门，里面的动物会叫，老虎叫会显示 ‘Wow !!’, 羊叫会显示 ‘mie~~’。 动物每叫一次体重减5斤。
    if choose == '敲门':
        if 'Tiger' == animal.name:
            print 'Wow !!'
            animal.weight -= 5
        elif 'Sheep' == animal.name:
            print 'mie~~'
            animal.weight -= 5

# 游戏者强记每个房间的动物是什么，以便不需要敲门就可以喂正确的食物。 游戏3分钟结束后，显示每个房间的动物和它们的体重。
    i += 1
for a in rooms:
    print '%s:%s' % (animal.name, animal.weight)